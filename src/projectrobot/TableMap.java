/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectrobot;

/**
 *
 * @author tud08
 */
public class TableMap {

    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void ShowMap() {
        System.out.println("Map");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (robot.isOn(x, y)) {
                    ShowRobot();
                } else if (bomb.isOn(x, y)) {
                    ShowBomb();
                } else {
                    ShowCell();
                }
            }
            System.out.println("");
        }
    }

    private void ShowCell() {
        System.out.print("-");
    }

    private void ShowBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void ShowRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}
