/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectrobot;

import java.util.Scanner;

/**
 *
 * @author tud08
 */
public class MainProgeam {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(2, 2, 'X', map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.ShowMap();
            char direction = inputDirection(kb);
            if(direction == 'q'){
                PrintByeBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void PrintByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);
    }
}
